# Svuick

[Svelte](https://svelte.dev) made easy

`Svelte Quick -> Svuick`

[svuick.netlify.app](https://svuick.netlify.app)

## Overview

-   **zero** pre-styling
-   simple, declarative apis
-   fully typed
-   ssr ready

## Packages

| Package                                       | Description                                                    |
| --------------------------------------------- | -------------------------------------------------------------- |
| [@svuick/action](packages/action)             | useful actions                                                 |
| [@svuick/contextmenu](packages/contextmenu)   | declarative contextmenus                                       |
| [@svuick/editor](packages/editor)             | wysiwyg editor                                                 |
| [@svuick/events](packages/events)             | pass events through the component tree                         |
| [@svuick/form](packages/form)                 | native form validation with zero styling                       |
| [@svuick/ide](packages/ide)                   | a web ide using monaco editor                                  |
| [@svuick/modal](packages/modal)               | declarative modals                                             |
| [@svuick/monaco](packages/monaco)             | monaco editor                                                  |
| [@svuick/pdf](packages/pdf)                   | pdf viewer                                                     |
| [@svuick/portal](packages/portal)             | attach elements anywhere                                       |
| [@svuick/scanner](packages/scanner)           | qrcode scanner and generator                                   |
| [@svuick/splitview](packages/splitview)       | splitview                                                      |
| [@svuick/store](packages/store)               | useful stores                                                  |
| [@svuick/tabs](packages/tabs)                 | tabs                                                           |
| [@svuick/time](packages/time)                 | display time absolute and relative in current browser language |
| [@svuick/toast](packages/toast)               | show toast messages                                            |
| [@svuick/virtual-list](packages/virtual-list) | render only visible items                                      |

## License

[MIT](./LICENSE)
