import { sveltekit } from '@sveltejs/kit/vite';
import { replaceCodePlugin } from 'vite-plugin-replace';
import getAvailableRoutes from './src/lib/getAvailableRoutes.js';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [
		sveltekit(),
		replaceCodePlugin({
			replacements: [
				{
					from: /GET_ROUTES\(["|']([\w\/\.\- ]+)["|'](,.*?)?\)/g,
					to(from, path, recursive) {
						const route = path.replace(/ /g, '');
						const routes = getAvailableRoutes(route, recursive?.includes('true'));
						return JSON.stringify(routes);
					}
				}
			]
		})
	]
};

export default config;
