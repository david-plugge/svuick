import path from 'path';
import { defineMDSveXConfig as defineConfig } from 'mdsvex';
import { getHighlighter } from 'shiki';
import rehypeSlug from 'rehype-slug';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';

function resolve(p) {
	const base = path.basename(process.cwd());
	if (base === 'docs') {
		return path.resolve(p);
	}
	return path.resolve('docs', p);
}

const config = defineConfig({
	extensions: ['.svx'],
	layout: {
		_: resolve('./src/routes/_page.svelte')
	},

	smartypants: {
		dashes: 'oldschool'
	},
	rehypePlugins: [rehypeSlug, rehypeAutolinkHeadings],
	highlight: {
		async highlighter(code, lang, meta) {
			const highlighter = await getHighlighter({
				theme: 'dark-plus'
			});
			let html = highlighter.codeToHtml(code, {
				lang
			});
			html = html.replace(/`/g, '&#96;').replace(/{/g, '&#123;').replace(/}/g, '&#125;');

			const classes = ['shiki'];

			if (meta?.includes('numbers')) {
				classes.push('shiki-line-numbers');
			}
			html = html.replace('class="shiki"', `class="${classes.join(' ')}"`);

			return html;
		}
	}
});

export default config;
