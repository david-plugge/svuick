const typography = require('@tailwindcss/typography');
const { cyan } = require('tailwindcss/colors');

/** @type {import('tailwindcss').Config} */
const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	darkMode: 'class',
	theme: {
		extend: {
			backgroundColor: {
				code: '#1e1e1e'
			},
			colors: {
				svuick: cyan
			}
		}
	},

	plugins: [typography]
};

module.exports = config;
