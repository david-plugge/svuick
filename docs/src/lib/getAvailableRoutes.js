import path from 'path';
import glob from 'glob';

const routesDir = './src/routes';
const routesDirAbs = path.resolve(routesDir);

/**
 * @param {string} dir
 * @param {boolean} recursive
 * @returns {Array<{ url: string, name: string}>}
 */
export default function getAvailableRoutes(dir, recursive) {
	const routes = glob
		.sync(`${routesDir}/**/*`)
		.map((r) => path.parse('/' + path.relative(routesDirAbs, r)))
		.filter(
			(r) =>
				!r.dir.split('/').some((p) => p.startsWith('_')) &&
				!r.name.startsWith('_') &&
				!!r.ext &&
				(recursive
					? r.dir.startsWith(dir)
					: dir === (r.name === 'index' ? path.dirname(r.dir) : r.dir))
		)
		.map((r) => {
			const name = r.name.replace(/@.*/, '');
			if (name === 'index') {
				return {
					name: path.basename(r.dir),
					url: r.dir
				};
			}
			return {
				name: name,
				url: path.join(r.dir, name)
			};
		});

	return routes;
}
