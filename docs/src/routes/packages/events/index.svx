---
title: '@svuick/events'
pkg: '@svuick/events'
---

# {title}

[![npm (scoped)](https://img.shields.io/npm/v/{pkg})](https://npmjs.com/package/{pkg})

## Installation

```bash
npm i @svuick/events
```

## What is this?

This package enables svelte developers to pass events through components with ease.

The main benefit is not having to forward events up the component tree.

## Usage

_src/lib/MyComponent.svelte_

```svelte
<script lang="ts">
    import { createEventContext } from '@svuick/events'
    import MyNestedComponent from './MyNestedComponent';

    const events = createEventContext();

    let value = 0;

    events.on('update', newValue => value = newValue);
</script>

<MyNestedComponent/>

{value}
```

_src/lib/MyComponent.svelte_

```svelte
<script lang="ts">
    import { getEventContext } from '@svuick/events'

    const events = getEventContext();

    let value = 0;

    function handleClick() {
        events.emit('update', value);
    }
</script>

<input type="number" bind:value={value}/>
<button on:click={handleClick}>Send!</button>
```
