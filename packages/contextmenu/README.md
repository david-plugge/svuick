# @svuick/contextmenu

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/contextmenu)](https://npmjs.com/package/@svuick/contextmenu)

## Install

```bash
npm i @svuick/contextmenu
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/contextmenu)

## LICENSE

Licensed under [MIT](../../LICENSE).
