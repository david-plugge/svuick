# @svuick/virtual-list

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/virtual-list)](https://npmjs.com/package/@svuick/virtual-list)

## Install

```bash
npm i @svuick/virtual-list
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/virtual-list)

## LICENSE

Licensed under [MIT](../../LICENSE).
