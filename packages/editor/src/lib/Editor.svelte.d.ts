import { SvelteComponentTyped } from 'svelte';
import type { EditorOptions, Editor as TipTapEditor } from '@tiptap/core';

declare const __propDef: {
	props: {
		class?: string;
		type?: 'html' | 'json' | 'text';
	} & Partial<
		Omit<EditorOptions, 'editorProps' | 'element'> & {
			editorProps: Omit<EditorOptions['editorProps'], 'attributes'>;
		}
	>;
	events: Record<string, never>;
	slots: {
		default: {
			editor: TipTapEditor;
		};
	};
};
export declare type EditorProps = typeof __propDef.props;
export declare type EditorEvents = typeof __propDef.events;
export declare type EditorSlots = typeof __propDef.slots;
export default class Editor extends SvelteComponentTyped<EditorProps, EditorEvents, EditorSlots> {}
export {};
