# @svuick/editor

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/editor)](https://npmjs.com/package/@svuick/editor)

## Install

```bash
npm i @svuick/editor
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/editor)

## LICENSE

Licensed under [MIT](../../LICENSE).
