# @svuick/modal

## 0.0.6

### Patch Changes

- disable page scroll by default

## 0.0.5

### Patch Changes

- use sveltekits App namespace
- Updated dependencies
  - @svuick/portal@0.0.2

## 0.0.4

### Patch Changes

- use publish config

## 0.0.3

### Patch Changes

- emit events on state change

## 0.0.2

### Patch Changes

- actually build package
