# @svuick/modal

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/modal)](https://npmjs.com/package/@svuick/modal)

## Install

```bash
npm i @svuick/modal
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/modal)

## LICENSE

Licensed under [MIT](../../LICENSE).
