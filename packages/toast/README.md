# @svuick/toast

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/toast)](https://npmjs.com/package/@svuick/toast)

## Install

```bash
npm i @svuick/toast
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/toast)

## LICENSE

Licensed under [MIT](../../LICENSE).
