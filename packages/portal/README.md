# @svuick/portal

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/portal)](https://npmjs.com/package/@svuick/portal)

## Install

```bash
npm i @svuick/portal
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/portal)

## LICENSE

Licensed under [MIT](../../LICENSE).
