import { setContext } from 'svelte';
import { derived, writable } from 'svelte/store';
import { extractErrors, findFormMembers, inputValue, isFormMember } from './helper';
import type { FormElement, InputValue, UseFormContext, UseFormItem } from './types';

interface FormValues {
	[key: string]: InputValue;
}

type UseFormOptions<T extends FormValues> = {
	[K in keyof T]?: {
		initial?: T[K];
		check?: (value: T[K]) => string | number | boolean | Promise<string | number | boolean>;
	};
};

export function useForm<T extends FormValues = FormValues>(options: UseFormOptions<T> = {}) {
	const items: Record<string, UseFormItem> = {};

	for (const name in options) {
		setDefaults(name);
	}

	let observer: MutationObserver;
	const itemsStore = writable(items);

	setContext<UseFormContext>('svuick-form', { items: itemsStore });

	function update() {
		itemsStore.set(items);
	}

	function setDefaults(nameOrNode: string | FormElement) {
		let name!: string;
		let node = undefined;
		if (typeof nameOrNode === 'string') {
			name = nameOrNode;
		} else if ('name' in nameOrNode) {
			name = nameOrNode.name;
			node = nameOrNode;
		}
		const value = options[name]?.initial;

		items[name] = {
			valid: true,
			errors: [],
			touched: false,
			initial: value,
			value: value as InputValue,
			node: node,
			hasTyped: false
		};
	}

	function handleInput(this: FormElement) {
		const item = items[this.name];
		item.value = inputValue(this);
		if (!item.hasTyped) {
			item.errors = [];
			item.valid = true;
		}
		item.hasTyped = true;

		update();
	}
	function handleBlur(this: FormElement) {
		const item = items[this.name];
		if (!item.hasTyped) return;
		item.errors = extractErrors(this);
		item.valid = item.errors.length === 0;
		item.value = inputValue(this);
		item.touched = true;
		item.hasTyped = false;
		update();
	}
	function handleInvalid(this: FormElement, e: Event) {
		e.preventDefault();
		const item = items[this.name];
		item.errors = extractErrors(this);
		item.valid = item.errors.length === 0;
		update();
	}
	function registerFormMembers(nodes: FormElement[]) {
		for (const node of nodes) {
			setDefaults(node);
			update();
			node.addEventListener('input', handleInput);
			node.addEventListener('blur', handleBlur);
			node.addEventListener('invalid', handleInvalid);
		}
	}
	function unregisterFormMembers(nodes: FormElement[]) {
		for (const node of nodes) {
			delete items[node.name];
			node.removeEventListener('input', handleInput);
			node.removeEventListener('blur', handleBlur);
			node.removeEventListener('invalid', handleInvalid);
		}
		update();
	}

	function setupFormObserver(form: HTMLFormElement) {
		observer = new MutationObserver(observeForm);
		observer.observe(form, { childList: true });
	}

	function observeForm(mutations: MutationRecord[]) {
		for (const mutation of mutations) {
			if (mutation.type === 'childList') {
				// If node gets removed
				mutation.removedNodes.forEach((node) => {
					if (node instanceof HTMLElement) {
						const elements = findFormMembers(node);
						if (isFormMember(node)) elements.push(node);
						unregisterFormMembers(elements);
					}
				});

				// If node gets added
				mutation.addedNodes.forEach((node) => {
					if (node instanceof HTMLElement) {
						const elements = findFormMembers(node);
						if (isFormMember(node)) elements.push(node);
						registerFormMembers(elements);
					}
				});
			}
		}
	}

	function form(node: HTMLFormElement) {
		const elements = findFormMembers(node);

		registerFormMembers(elements);
		setupFormObserver(node);

		node.addEventListener('submit', (e) => {
			e.preventDefault();
			e.stopPropagation();
		});

		return {
			destroy() {
				if (observer) observer.disconnect();
			}
		};
	}

	const formStore = derived<
		typeof itemsStore,
		{
			values: T;
			valid: boolean;
		}
	>(itemsStore, ($items, set) => {
		const values = {} as FormValues;
		let valid = true;
		for (const name in $items) {
			values[name] = $items[name].value;
			valid = valid && $items[name].errors.length === 0;
		}
		set({ values: values as T, valid });
	});

	form.subscribe = formStore.subscribe;
	return form;
}
