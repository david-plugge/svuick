import type { FormElement, InputValue, NativeInputError } from './types';

export function findFormMembers(node: HTMLElement): FormElement[] {
	return [
		...findChildrenByTag(node, 'input'),
		...findChildrenByTag(node, 'textarea'),
		...findChildrenByTag(node, 'select')
	] as FormElement[];
}

export function findChildrenByTag(node: HTMLElement, tag: string): Element[] {
	return Array.from(node.getElementsByTagName(tag));
}

export function isFormMember(node: HTMLElement): node is FormElement {
	return (
		node instanceof HTMLInputElement ||
		node instanceof HTMLTextAreaElement ||
		node instanceof HTMLSelectElement
	);
}

export function inputValue(node: FormElement): InputValue {
	return node.type === 'number'
		? Number(node.value)
		: node.type === 'checkbox'
		? node.checked
		: node.value;
}

export function extractErrors(node: FormElement): NativeInputError[] {
	const validity = node.validity;
	const errors: NativeInputError[] = [];

	if (validity.valueMissing) errors.push('required');
	if (validity.typeMismatch) errors.push('type');
	if (validity.patternMismatch) errors.push('pattern');
	if (validity.rangeOverflow) errors.push('max');
	if (validity.rangeUnderflow) errors.push('min');
	if (validity.stepMismatch) errors.push('step');
	if (validity.tooLong) errors.push('maxLength');
	if (validity.tooShort) errors.push('minLength');

	return errors;
}
