import type { Writable } from 'svelte/store';

export type InputValue = string | number | boolean;

/* eslint-disable @typescript-eslint/ban-types */
export type NativeInputError =
	| 'required'
	| 'type'
	| 'min'
	| 'max'
	| 'step'
	| 'minLength'
	| 'maxLength'
	| 'pattern'
	| String;

export type InputErrorFunction = (value: any) => boolean | Promise<boolean>;

export type FormElement = HTMLInputElement & HTMLTextAreaElement & HTMLSelectElement;
export type UseFormItem = {
	errors: NativeInputError[];
	valid: boolean;
	touched: boolean;
	initial: any;
	node?: FormElement;
	value: InputValue;
	hasTyped: boolean;
};

export type UseFormContext = {
	items: Writable<Record<string, UseFormItem>>;
};
