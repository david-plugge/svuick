# @svuick/form

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/form)](https://npmjs.com/package/@svuick/form)

## Install

```bash
npm i @svuick/form
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/form)

## LICENSE

Licensed under [MIT](../../LICENSE).
