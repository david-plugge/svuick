# @svuick/ide

## 0.0.3

### Patch Changes

- Updated dependencies
  - @svuick/monaco@0.0.3

## 0.0.2

### Patch Changes

- Updated dependencies
  - @svuick/events@0.0.3
