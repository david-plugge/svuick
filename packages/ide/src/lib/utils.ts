import { getContext } from 'svelte';
import { getEventContext } from '@svuick/events';
import type { IdeContext, IdeEvents } from './types';

export function getIdeContext() {
	const context = getContext<IdeContext>('SVUICK_IDE');
	const events = getEventContext<IdeEvents>('SVUICK_IDE_EVENTS');
	return {
		...context,
		events
	};
}
