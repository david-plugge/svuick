import type { SvelteComponent } from 'svelte';
import type { Writable } from 'svelte/store';

export type EditorMatcher = {
	name: string;
	pattern: string | string[];
	component: typeof SvelteComponent;
};

export type IdeFileMap = Record<string, string>;

export type IdeFile = {
	path: string;
	content: string;
};

export type IdeConsoleItem<T = unknown> = {
	type: string;
	data: T;
};

export type IdeEvents = {
	// files
	file_open: (file: IdeFile) => void;
	file_added: (file: IdeFile) => void;
	file_removed: (file: IdeFile) => void;
	file_changed: (file: IdeFile) => void;

	// console
	console_new_item: (item: IdeConsoleItem) => void;
};

export type IdeContext = {
	files: Writable<IdeFileMap>;
	currentFile: Writable<string>;
};
