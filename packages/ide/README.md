# @svuick/ide

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/ide)](https://npmjs.com/package/@svuick/ide)

## Install

```bash
npm i @svuick/ide
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/ide)

## LICENSE

Licensed under [MIT](../../LICENSE).
