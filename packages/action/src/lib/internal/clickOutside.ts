function clickOutside(node: HTMLElement): { destroy: () => void } {
	function handler(e: MouseEvent) {
		const target = e.target as Node;
		if (!node.contains(target) && !e.defaultPrevented) {
			node.dispatchEvent(new CustomEvent('click_outside'));
		}
	}
	window.addEventListener('click', handler, true);
	return {
		destroy() {
			window.removeEventListener('click', handler, true);
		}
	};
}

export { clickOutside };
