let intersectionObserver: IntersectionObserver;

function observeIntersection(node: HTMLElement): void {
	if (!intersectionObserver)
		intersectionObserver = new IntersectionObserver((entries) => {
			entries.forEach((entry) => {
				const eventType = entry.isIntersecting ? 'enter_viewport' : 'exit_viewport';
				entry.target.dispatchEvent(new CustomEvent(eventType));
			});
		});
	intersectionObserver.observe(node);
}
function unobserveIntersection(node: HTMLElement): void {
	if (!intersectionObserver) return;
	intersectionObserver.unobserve(node);
}

function viewport(node: HTMLElement): { destroy: () => void } {
	observeIntersection(node);
	return {
		destroy() {
			unobserveIntersection(node);
		}
	};
}

export { viewport };
