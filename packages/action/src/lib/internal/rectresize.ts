let resizeObserver: ResizeObserver;

function observeResize(node: HTMLElement, options?: ResizeObserverOptions): void {
	if (!resizeObserver)
		resizeObserver = new ResizeObserver((entries) => {
			entries.forEach((entry) => {
				entry.target.dispatchEvent(new CustomEvent('rectresize', { detail: entry.contentRect }));
			});
		});
	resizeObserver.observe(node, options);
}
function unobserveResize(node: HTMLElement): void {
	if (!resizeObserver) return;
	resizeObserver.unobserve(node);
}

function rectresize(
	node: HTMLElement,
	options?: ResizeObserverBoxOptions
): { destroy: () => void } {
	observeResize(node, { box: options });
	return {
		destroy() {
			unobserveResize(node);
		}
	};
}

export { rectresize };
