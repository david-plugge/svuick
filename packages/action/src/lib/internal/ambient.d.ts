declare namespace svelte.JSX {
	/* eslint-disable @typescript-eslint/no-unused-vars */
	interface HTMLProps<T> {
		onenter_viewport?: (
			event: CustomEvent<DOMRectReadOnly> & { currentTarget: EventTarget & T }
		) => void;
		onexit_viewport?: (
			event: CustomEvent<DOMRectReadOnly> & { currentTarget: EventTarget & T }
		) => void;
		onclick_outside?: (
			event: CustomEvent<DOMRectReadOnly> & { currentTarget: EventTarget & T }
		) => void;
		onrectresize?: (
			event: CustomEvent<DOMRectReadOnly> & { currentTarget: EventTarget & T }
		) => void;
	}
}
