import { tick } from 'svelte';

const FOCUSABLE_ELEMENTS = [
	'a[href]',
	'area[href]',
	'input:not([disabled]):not([type="hidden"]):not([aria-hidden])',
	'select:not([disabled]):not([aria-hidden])',
	'textarea:not([disabled]):not([aria-hidden])',
	'button:not([disabled]):not([aria-hidden])',
	'iframe',
	'object',
	'embed',
	'[contenteditable]',
	'[tabindex]:not([tabindex^="-"])'
];

function getFocusableElements(node: HTMLElement): HTMLElement[] {
	return Array.from(node.querySelectorAll<HTMLElement>(FOCUSABLE_ELEMENTS.join(', '))).filter(
		(item) => {
			const computedStyle = getComputedStyle(item, null);
			return (
				item instanceof HTMLElement &&
				computedStyle.getPropertyValue('display') !== 'none' &&
				computedStyle.getPropertyValue('visibility') !== 'hidden'
			);
		}
	);
}

function isTab(e: KeyboardEvent): boolean {
	return e.key === 'Tab' || e.code === 'Key' || e.keyCode === 9;
}

function innerfocus(node: HTMLElement): { destroy: () => void } {
	let first: HTMLElement;
	let last: HTMLElement;

	function firstChild(e: KeyboardEvent) {
		if (isTab(e) && e.shiftKey) {
			e.preventDefault();
			last.focus();
		}
	}
	function lastChild(e: KeyboardEvent) {
		if (isTab(e) && !e.shiftKey) {
			e.preventDefault();
			first.focus();
		}
	}

	function setup() {
		cleanup();
		const focusable = getFocusableElements(node);
		if (focusable.length < 1) return;

		first = focusable[0];
		last = focusable[focusable.length - 1] as HTMLElement;

		first.addEventListener('keydown', firstChild);
		last.addEventListener('keydown', lastChild);
	}

	function cleanup() {
		first && first.removeEventListener('keydown', firstChild);
		last && last.removeEventListener('keydown', lastChild);
	}

	const observer = new MutationObserver(() => {
		setup();
	});

	setup();
	observer.observe(node, { childList: true });

	return {
		destroy() {
			observer.disconnect();
			cleanup();
		}
	};
}

function focus(node: HTMLElement): void {
	tick().then(() => {
		node.focus();
	});
}

export { focus, innerfocus };
