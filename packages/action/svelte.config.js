import adapter from '@sveltejs/adapter-auto';
import preprocess from 'svelte-preprocess';
import { resolve } from 'path';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter(),
		package: {
			/**
			 * @param {string} filepath
			 * @returns {boolean}
			 */
			exports(filepath) {
				return !filepath.startsWith('internal');
			}
		},
		alias: {
			'@svuick/action': resolve('src/lib')
		}
	}
};

export default config;
