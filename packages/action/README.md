# @svuick/action

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/action)](https://npmjs.com/package/@svuick/action)

## Install

```bash
npm i @svuick/action
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/action)

## LICENSE

Licensed under [MIT](../../LICENSE).
