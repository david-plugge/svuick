# @svuick/action

## 0.0.4

### Patch Changes

- new api

## 0.0.3

### Patch Changes

- use sveltekits App namespace

## 0.0.2

### Patch Changes

- fix: add .js extension to imports
