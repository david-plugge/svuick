# @svuick/tabs

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/tabs)](https://npmjs.com/package/@svuick/tabs)

## Install

```bash
npm i @svuick/tabs
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/tabs)

## LICENSE

Licensed under [MIT](../../LICENSE).
