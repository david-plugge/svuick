# @svuick/monaco

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/monaco)](https://npmjs.com/package/@svuick/monaco)

## Install

```bash
npm i @svuick/monaco
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/monaco)

## LICENSE

Licensed under [MIT](../../LICENSE).
