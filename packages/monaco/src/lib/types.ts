export type ContentChangeEvent = CustomEvent<{ path: string; content: string }>;
export type ModelNotFoundEvent = CustomEvent<{ path: string }>;
