export { default, default as TextEditor } from './TextEditor.svelte';
export { setupMonaco, cleanupMonaco } from './monaco';
export { createModel, removeModel } from './monaco/helper';
export type { IMonaco, EditorConfig } from './monaco/types';
export type { ContentChangeEvent, ModelNotFoundEvent } from './types';
