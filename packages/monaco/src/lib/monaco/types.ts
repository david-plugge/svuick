import type IMonaco from 'monaco-editor';

export type BuiltinWorkers = 'html' | 'css' | 'json' | 'yaml' | 'typescript';

export type { IMonaco };
export type Monaco = typeof IMonaco;
export type Uri = IMonaco.Uri;
export type MonacoEditor = IMonaco.editor.IStandaloneCodeEditor;

export type EditorState = Map<string, IMonaco.editor.ICodeEditorViewState>;

export type EditorConfig = Omit<
	IMonaco.editor.IStandaloneEditorConstructionOptions,
	'model' | 'automaticLayout'
>;

export type WorkerFactory = new () => Worker;
