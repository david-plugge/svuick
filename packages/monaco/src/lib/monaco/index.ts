import type { EditorState, Monaco, WorkerFactory } from './types';
import { writable } from 'svelte/store';

export const monacoReady = writable(false);

let monaco: Monaco;
const editorState: EditorState = new Map();

interface CleanMonacoOptions {
	disposeViewstate?: boolean;
}

interface SetupOptions {
	onReady?: (monaco: Monaco) => void;
	workers?: Record<string, WorkerFactory>;
}

const WORKER_IMPORT_MAP = {
	editorWorkerService: () => import('monaco-editor/esm/vs/editor/editor.worker?worker'),
	json: () => import('monaco-editor/esm/vs/language/json/json.worker?worker'),
	typescript: () => import('monaco-editor/esm/vs/language/typescript/ts.worker?worker'),
	html: () => import('monaco-editor/esm/vs/language/html/html.worker?worker'),
	yaml: () => import('./yaml.worker?worker')
};

const WORKER_ALIAS_MAP: Record<string, string> = {
	typescript: 'javascript',
	javascript: 'typescript'
};

async function importWorkers(): Promise<Record<string, WorkerFactory>> {
	const worker_map: Record<string, WorkerFactory> = {};

	await Promise.all(
		Object.entries(WORKER_IMPORT_MAP).map(([name, importer]) =>
			importer().then((mod) => {
				worker_map[name] = mod.default;
			})
		)
	);

	return worker_map;
}

async function _setup(options: SetupOptions) {
	const DEFAULT_WORKERS = await importWorkers();

	const worker_map = {
		...DEFAULT_WORKERS,
		...(options.workers ?? {})
	};

	// @ts-ignore
	window.MonacoEnvironment = {
		getWorker: function (_moduleId: any, label: string) {
			const WorkerConstructor = worker_map[label] ?? worker_map[WORKER_ALIAS_MAP[label]];

			if (WorkerConstructor) {
				return new WorkerConstructor();
			}
			throw new Error(`Unknown label ${label}`);
		}
	};

	monaco = await import('monaco-editor');
	if (typeof options.onReady === 'function') {
		await options.onReady(monaco);
	}
	monacoReady.set(true);
}

export function setupMonaco(options: SetupOptions = {}) {
	if (typeof window === 'undefined') throw new Error('setupMonaco called on the server');

	_setup(options);

	return cleanupMonaco;
}

export function getMonaco() {
	if (typeof monaco === 'undefined') throw new Error('used monaco before setupMonaco was called');
	return {
		monaco,
		editorState
	};
}

export async function cleanupMonaco(options: CleanMonacoOptions = {}) {
	if (!monaco) return;
	monacoReady.set(false);

	monaco.editor.getModels().forEach((model) => {
		model.dispose();
	});
	if (options.disposeViewstate) {
		editorState.clear();
	}
}
