import { getMonaco } from './index';

export function updateLanguage(path: string, language: string) {
	const {
		monaco: { editor, Uri }
	} = getMonaco();

	const uri = Uri.parse(path);
	const model = editor.getModel(uri);

	if (model) {
		editor.setModelLanguage(model, language);
	}
}

export function createModel(path: string, content: string, language?: string) {
	const {
		monaco: { Uri, editor }
	} = getMonaco();

	const uri = Uri.parse(path);
	if (!editor.getModel(uri)) {
		editor.createModel(content, language, uri);
	}
}

export function removeModel(path: string) {
	const {
		monaco: { Uri, editor }
	} = getMonaco();

	const uri = Uri.parse(path);
	const model = editor.getModel(uri);
	if (model) {
		model.dispose();
	}
}
