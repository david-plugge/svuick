# @svuick/pdf

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/pdf)](https://npmjs.com/package/@svuick/pdf)

## Install

```bash
npm i @svuick/pdf
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/pdf)

## LICENSE

Licensed under [MIT](../../LICENSE).
