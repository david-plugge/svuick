export { default as Detector } from './CodeDetector.svelte';
export { default as Generator } from './QRCodeGenerator.svelte';
