export {};

let barcodeDetector: BarcodeDetector;
let runs = 0;

addEventListener('message', async (ev) => {
	if (ev.data.type === 'init') {
		barcodeDetector = new BarcodeDetector({ formats: ev.data.format });
	} else if (ev.data.type === 'data') {
		try {
			const codes = await barcodeDetector.detect(ev.data.source);
			if (codes.length)
				codes.forEach((code) => {
					postMessage({ type: 'code', code, runs });
				});
			runs++;
		} catch (err) {}
	}
});
