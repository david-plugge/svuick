import type { Writable } from 'svelte/store';

export type CornerPoint = {
	x: number;
	y: number;
};

export type ScannerContext = Writable<{
	size: { width: number; height: number } | null;
	result: BarcodeDetectorResult | null;
}>;

export type BarcodeDetectorResult = {
	boundingBox: DOMRectReadOnly;
	cornerPoints: [CornerPoint, CornerPoint, CornerPoint, CornerPoint];
	format: FORMAT;
	rawValue: string;
};
export type FORMAT =
	| 'aztec'
	| 'code_128'
	| 'code_39'
	| 'code_93'
	| 'codabar'
	| 'data_matrix'
	| 'ean_13'
	| 'ean_8'
	| 'itf'
	| 'pdf417'
	| 'qr_code'
	| 'upc_a'
	| 'upc_e'
	| 'unknown';
export type FACING_MODE = 'user' | 'environment';

declare global {
	class BarcodeDetector {
		constructor(options?: { formats: FORMAT[] });
		getSupportedFormats(): Promise<FORMAT[]>;
		detect(image: ImageBitmapSource): Promise<BarcodeDetectorResult[]>;
	}
	interface Window {
		BarcodeDetector: {};
	}
}
