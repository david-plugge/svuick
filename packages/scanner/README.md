# @svuick/scanner

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/scanner)](https://npmjs.com/package/@svuick/scanner)

## Install

```bash
npm i @svuick/scanner
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/scanner)

## LICENSE

Licensed under [MIT](../../LICENSE).
