# @svuick/events

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/events)](https://npmjs.com/package/@svuick/events)

## Install

```bash
npm i @svuick/events
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/events)

## LICENSE

Licensed under [MIT](../../LICENSE).
