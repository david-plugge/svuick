import { getContext, onDestroy, setContext } from 'svelte';
import { EventEmitter, type EventMap } from './events';

function wrapContextEvents<Events extends EventMap = EventMap>(emitter: EventEmitter<Events>) {
	let unsubscribers: Array<{ type: keyof Events; callback: Events[keyof Events] }> = [];

	onDestroy(() => {
		unsubscribers.forEach(({ type, callback }) => emitter.off(type, callback));
		unsubscribers = [];
	});

	const ctx = {
		on<K extends keyof Events>(type: K, callback: Events[K]) {
			emitter.on(type, callback);
			unsubscribers.push({ type, callback });
			return ctx;
		},
		off<K extends keyof Events>(type: K, callback: Events[K]) {
			unsubscribers = unsubscribers.filter((u) => !(u.type === type && u.callback === callback));
			emitter.off(type, callback);
			return ctx;
		},
		emit<K extends keyof Events>(type: K, ...args: Parameters<Events[K]>) {
			emitter.emit(type, ...args);
			return ctx;
		}
	};
	return ctx;
}

export function createEventContext<Events extends EventMap = EventMap>(key = 'EVENTS') {
	const target = new EventEmitter();
	setContext(key, target);
	return wrapContextEvents<Events>(target);
}
export function getEventContext<Events extends EventMap = EventMap>(key = 'EVENTS') {
	return wrapContextEvents<Events>(getContext(key));
}
