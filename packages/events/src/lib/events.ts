/* eslint-disable @typescript-eslint/no-explicit-any */
export type Listener = (...args: any[]) => void | Promise<void>;

export interface EventMap {
	[key: string]: Listener;
}

function addArrayMap<K = keyof EventMap>(map: Map<K, Set<Listener>>, key: K, cb: Listener): void {
	const set = map.get(key);
	if (!set) {
		map.set(key, new Set([cb]));
	}
	set?.add(cb);
}
function delArrayMap<K = keyof EventMap>(map: Map<K, Set<Listener>>, key: K, cb: Listener): void {
	const set = map.get(key);
	if (!set) return;
	if (set.size <= 1) map.delete(key);
	else set.delete(cb);
}

export class EventEmitter<Events extends EventMap> {
	#listener = new Map<keyof Events, Set<Listener>>();
	#onceListener = new Map<keyof Events, Set<Listener>>();

	on<K extends keyof Events = keyof Events>(type: K, listener: Events[K]) {
		addArrayMap(this.#listener, type, listener);
		return this;
	}
	once<K extends keyof Events = keyof Events>(type: K, listener: Events[K]) {
		addArrayMap(this.#onceListener, type, listener);
		return this;
	}
	off<K extends keyof Events = keyof Events>(type: K, listener: Events[K]) {
		delArrayMap(this.#listener, type, listener);
		delArrayMap(this.#onceListener, type, listener);
		return this;
	}
	emit<K extends keyof Events = keyof Events>(type: K, ...args: Parameters<Events[K]>) {
		let listeners = this.#listener.get(type);
		if (listeners) {
			listeners.forEach((l) => l(...(args as any)));
		}
		listeners = this.#onceListener.get(type);
		if (listeners) {
			this.#onceListener.delete(type);
			listeners.forEach((l) => l(...(args as any)));
		}
		return this;
	}
}
