# @svuick/store

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/store)](https://npmjs.com/package/@svuick/store)

## Install

```bash
npm i @svuick/store
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/store)

## LICENSE

Licensed under [MIT](../../LICENSE).
