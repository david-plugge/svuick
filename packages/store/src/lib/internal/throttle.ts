import { derived, type Readable } from 'svelte/store';

// throttle a store
function throttle<T>(store: Readable<T>, delay = 100): Readable<T> {
	let lastTime: number;
	return derived(store, ($value, set) => {
		const now = Date.now();
		if (!lastTime || now - lastTime > delay) {
			set($value);
			lastTime = now;
		} else {
			const timeout = setTimeout(() => {
				set($value);
			}, delay);
			return () => {
				clearTimeout(timeout);
			};
		}
	});
}

export { throttle };
