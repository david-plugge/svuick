import { get, writable, type StartStopNotifier, type Writable } from 'svelte/store';

const browser = typeof window !== 'undefined';

type PersistentStorageAdapter = Pick<Storage, 'getItem' | 'setItem'>;

function persistentStore<T>(
	key: string,
	value: T,
	storage: PersistentStorageAdapter,
	start: StartStopNotifier<T>
): Writable<T> {
	const store = writable(value, start);
	if (!browser) return store;

	const storedValue = storage.getItem(key);
	if (storedValue != null) store.set(JSON.parse(storedValue));

	store.subscribe((val) => {
		storage.setItem(key, JSON.stringify(val));
	});
	if (storage === window.localStorage) {
		window.addEventListener('storage', (ev) => {
			if (ev.key !== key) return;
			const newValue = typeof ev.newValue === 'string' ? JSON.parse(ev.newValue) : null;
			if (get(store) !== newValue) store.set(newValue);
		});
	}

	return store;
}

const noStore: PersistentStorageAdapter = {
	getItem: () => null,
	setItem: () => undefined
};

function localStore<T>(key: string, value: T, start: StartStopNotifier<T>): Writable<T> {
	return persistentStore(key, value, browser ? localStorage : noStore, start);
}
function sessionStore<T>(key: string, value: T, start: StartStopNotifier<T>): Writable<T> {
	return persistentStore(key, value, browser ? sessionStorage : noStore, start);
}

export { localStore, sessionStore, persistentStore };
