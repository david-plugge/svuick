import { derived, type Readable } from 'svelte/store';

// debounce a store
function debounce<T>(store: Readable<T>, delay = 100): Readable<T> {
	let initialised = false;
	return derived(store, ($value, set) => {
		if (!initialised) {
			set($value);
			initialised = true;
			return;
		}
		const timeout = setTimeout(() => {
			set($value);
		}, delay);
		return () => {
			clearTimeout(timeout);
		};
	});
}

export { debounce };
