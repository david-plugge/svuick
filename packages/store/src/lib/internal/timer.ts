import type { Readable } from 'svelte/store';

/* eslint-disable @typescript-eslint/no-empty-function */
function noop() {}

// interval store for automated cleanup
const interval: ((handler: TimerHandler, timeout?: number, ...args: unknown[]) => () => void) &
	Readable<(handler: TimerHandler, timeout?: number, ...args: unknown[]) => void> = (
	handler,
	timeout,
	...args
) => {
	const handle = setInterval(handler, timeout, ...args);
	return () => {
		clearInterval(handle);
	};
};

interval.subscribe = (run) => {
	let unsub = noop;

	run((...args) => {
		unsub = interval(...args);
	});

	return () => unsub();
};

// timeout store for automated cleanup
const timeout: ((handler: TimerHandler, timeout?: number, ...args: unknown[]) => () => void) &
	Readable<(handler: TimerHandler, timeout?: number, ...args: unknown[]) => void> = (
	handler,
	timeout,
	...args
) => {
	const handle = setTimeout(handler, timeout, ...args);
	return () => {
		clearTimeout(handle);
	};
};

timeout.subscribe = (run) => {
	let unsub = noop;
	run((...args) => {
		unsub = timeout(...args);
	});
	return () => unsub();
};

export { timeout, interval };
