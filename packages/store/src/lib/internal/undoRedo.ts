import { get, type Readable, type Writable } from 'svelte/store';

// Undo-Redo store
type JsonPrimitive = string | number | boolean | null;
type JsonMap = {
	[key: string]: JsonPrimitive | JsonMap | JsonArray;
};
type JsonArray = Array<JsonPrimitive | JsonMap | JsonArray>;
type JsonType = JsonPrimitive | JsonMap | JsonArray;

function clone<T extends JsonType>(obj: T): T {
	return JSON.parse(JSON.stringify(obj));
}

type UndoRedoStore<T extends JsonType> = Readable<T> & {
	set(value: T): void;
	undo(): void;
	redo(): void;
};

function undoRedo<T extends JsonType>(store: Writable<T>): UndoRedoStore<T> {
	const history: T[] = [clone(get(store))];
	let historyIndex = 0;

	const updateStore = () => store.set(clone(history[historyIndex]));

	return {
		subscribe: store.subscribe,
		set(value: T) {
			historyIndex++;
			history[historyIndex] = clone(value);
			history.splice(historyIndex + 1);
			updateStore();
		},
		undo() {
			if (historyIndex > 0) {
				historyIndex--;
				updateStore();
			}
		},
		redo() {
			if (historyIndex < history.length - 1) {
				historyIndex++;
				updateStore();
			}
		}
	};
}

// Undo-Redo store using actions
interface Action<T> {
	apply(value: T): T;
	reverse(value: T): T;
}

type UndoRedoActionStore<T> = Readable<T> & {
	doAction(action: Action<T>): void;
	undo(): void;
	redo(): void;
};
function undoRedoAction<T>(store: Writable<T>): UndoRedoActionStore<T> {
	const history: Action<T>[] = [];
	let historyIndex = 0;

	return {
		subscribe: store.subscribe,
		undo() {
			if (historyIndex >= 0) {
				store.update((value) => history[historyIndex].reverse(value));
				historyIndex--;
			}
		},
		redo() {
			if (historyIndex < history.length - 1) {
				historyIndex++;
				store.update((value) => history[historyIndex].apply(value));
			}
		},
		doAction(action: Action<T>) {
			historyIndex++;
			history[historyIndex] = action;
			history.splice(historyIndex + 1);
			store.update((value) => history[historyIndex].apply(value));
		}
	};
}

export type { UndoRedoStore, UndoRedoActionStore };
export { undoRedo, undoRedoAction };
