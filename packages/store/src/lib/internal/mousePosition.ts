import { readable, type Readable } from 'svelte/store';

const browser = typeof window !== 'undefined';

function _mousePosition(): Readable<{ x: number; y: number }> {
	return readable({ x: 0, y: 0 }, (set) => {
		if (!browser) return;
		document.body.addEventListener('mousemove', handleMove);

		function handleMove(e: MouseEvent) {
			set({
				x: e.clientX,
				y: e.clientY
			});
		}
		return () => {
			document.body.removeEventListener('mousemove', handleMove);
		};
	});
}

export const mousePosition = _mousePosition();
