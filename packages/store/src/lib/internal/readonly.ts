import type { Readable } from 'svelte/store';

// make a store readonly
function readonly<T>(store: Readable<T>): Readable<T> {
	return {
		subscribe: store.subscribe
	};
}
export { readonly };
