# @svuick/splitview

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/splitview)](https://npmjs.com/package/@svuick/splitview)

## Install

```bash
npm i @svuick/splitview
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/splitview)

## LICENSE

Licensed under [MIT](../../LICENSE).
