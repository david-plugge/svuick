export function clamp(min: number, max: number, num: number) {
	return Math.min(max, Math.max(min, num));
}

export function round(num: number, steps = 0) {
	const mul = 10 ** steps;
	return Math.round(num * mul) / mul;
}
