import type { SvelteComponent } from 'svelte';

export type SplitViewItem<T extends typeof SvelteComponent = typeof SvelteComponent> = {
	size: number;
	minSize?: number;
	component?: T;
	props?: InstanceType<T>['$$prop_def'];
	direction?: 'horizontal' | 'vertical';
	items?: SplitViewItem[];
};
