declare namespace svelte.JSX {
	/* eslint-disable @typescript-eslint/no-unused-vars */
	interface HTMLProps<T> {
		onpanstart?: (
			event: CustomEvent<{ x: number; y: number }> & { currentTarget: EventTarget & T }
		) => void;
		onpanmove?: (
			event: CustomEvent<{ x: number; y: number; dx: number; dy: number }> & {
				currentTarget: EventTarget & T;
			}
		) => void;
		onpanend?: (
			event: CustomEvent<{ x: number; y: number }> & { currentTarget: EventTarget & T }
		) => void;
	}
}
