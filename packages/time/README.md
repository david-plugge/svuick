# @svuick/time

[![npm (scoped)](https://img.shields.io/npm/v/@svuick/time)](https://npmjs.com/package/@svuick/time)

## Install

```bash
npm i @svuick/time
```

## Documentation

[Read the docs](https://svuick.netlify.app/packages/time)

## LICENSE

Licensed under [MIT](../../LICENSE).
