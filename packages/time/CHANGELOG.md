# @svuick/time

## 0.0.5

### Patch Changes

- use sveltekits App namespace

## 0.0.4

### Patch Changes

- update to svelte kit 1.0.0-next.242
