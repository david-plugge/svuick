# TODO

## docs

    -   action
    -   editor
    -   events
    -   form
    -   highlight
    -   modal
    -   monaco
    -   mq ???
    -   pdf
    -   portal
    -   store
    -   tabs
    -   time
    -   toast
    -   utils ???

## todo

-   action
    -   remove dnd
-   editor
    -   move tiptap code to external file
-   events
-   form
    -   refactor
-   highlight
    -   refactor
-   modal
    -   refactor
-   monaco
    -   add yaml support
    -   file explorer
-   mq
    -   remove ???
-   pdf
    -   refactor
-   portal
    -   refactor
-   store
    -   refactor
-   tabs
    -   refactor
-   time
    -   refactor
-   toast
    -   refactor
-   utils
    -   remove ???

## packages

-   tags
-   multiselect
-   autocomplete
